from flask import Flask, request ,render_template
import requests
app = Flask(__name__,static_folder='public/comprarTicket1_files',)

@app.route('/enlaces')
def enlaces():
	enlaces=[{"url":"http://www.google.es","texto":"Google"},
			{"url":"http://www.twitter.com","texto":"Twitter"},
			{"url":"http://www.facbook.com","texto":"Facebook"},
			]
	return render_template("template4.html",enlaces=enlaces)

@app.route('/hello/<nombre>')
def saluda(nombre=None):
    return render_template("comprarTicket.html", nombre=nombre)


@app.route("/buyTikect/<nombrePelicula>")
def peliculas(nombrePelicula=None):
	# https://www.omdbapi.com/?t=batman&&apikey=521aa8b2
	response = requests.get('https://www.omdbapi.com/?t={}&&apikey=521aa8b2'.format(nombrePelicula))
	print(response)
	return render_template("comprarTicket1.html",response=response.json())




@app.route('/articulos/new',methods=["POST"])
def articulos_new():
	return 'Está URL recibe información de un formulario con el método POST'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return 'Hemos accedido con POST'
    else:
        return 'Hemos accedido con GET'


@app.route("/suma",methods=["GET","POST"])
def sumar():
	if request.method=="POST":
		num1=request.form.get("num1")
		num2=request.form.get("num2")
		return str(int(num1)+int(num2))
	else:
		return '''<from action="/suma" method="POST">
				<label>N1:</label>
				<input type="text" name="num1"/>
				<label>N2:</label>
				<input type="text" name="num2"/>
                <button type="submit">Envíe su mensaje</button>
				</form>'''


if __name__ == '__main__':
    app.run(debug=True, port=4000)
    